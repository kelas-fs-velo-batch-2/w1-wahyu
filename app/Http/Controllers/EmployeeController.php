<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Employee::get();
        $count = Employee::count();
        return response()->json([
            'data'  => $model,
            'count' => $count,
            'success' => true
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => ['required'],
            'date_of_birth' => ['required','date'],
            'job_title'     => ['required']
        ],[
            'name.required' => 'Nama Disi',
            'date_of_birth.required' => 'Tanggal Lahir Diisi',
            'date_of_birth.date'     => 'Format Tanggal Tidak Valid',
            'job_title.required'     => 'Job Title Diisi'
        ]);

        $model = new Employee;
        $model->name            = $request->name;
        $model->date_of_birth   = $request->date_of_birth;
        $model->job_title       = $request->job_title;
        $model->profiles        = [];
        $model->save();
        
        return response()->json([
            'success'   => true,
            'data'      => $model
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return response()->json([
            'data'    => $employee,
            'success' => true
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->name            = $request->name;
        $employee->date_of_birth   = $request->date_of_birth;
        $employee->job_title       = $request->job_title;
        $employee->save();
        
        return response()->json([
            'success'   => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return response()->json([
            'success'   => true
        ]);
    }
}
