<?php

namespace Database\Seeders;

use App\Models\Unit;
use Illuminate\Database\Seeder;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hrd = Unit::create([
            'name'          => 'HRD',
            'parent_unit_id'=> 0 
        ]);

        Unit::create([
            'name'          => 'Human Resource',
            'parent_unit_id'=> $hrd->id 
        ]);

        Unit::create([
            'name'          => 'Career Development',
            'parent_unit_id'=> $hrd->id
        ]);
    }
}
