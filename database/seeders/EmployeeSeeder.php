<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::create([
            'name'  => 'Wahyu',
            'date_of_birth' => '1993-05-10',
            'job_title'     => 'Kepala Sekolah',
            'profiles'      => []
        ]);
    }
}
